<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contact_us extends MX_Controller
{

	function __construct() {
            $this->load->helper('captcha');
		$this->load->model('mdl_contact_us');
		parent::__construct();
	}


	function index(){
		$submit = $this->input->post('submit', TRUE);
		if($submit=="Submit"){
			//person has submitted the form
			$data= $this->get_data_from_post();
		} 
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}			              
		$vals = array(
			'img_path'	 => './captcha/',
			'img_url'	 => base_url().'captcha/',
			'img_width'  => '250', 
			'img_height'  => '40',
			'font_path'=>'./design/admin/font/rosemary.ttf'
			);
		$cap = create_captcha($vals);
		$data1['word']=$cap['word'];
		$data['query'] = $this->get('id');
		$data['word']=$data1['word'];
		$data['image']=$cap['image'];					
		$data['view_file']="table";
		$this->load->module('template');
		$this->template->front($data);
	}
    
	function details()
	{ 
		
                
               
		$data['view_file']="form";
		$this->load->module('template');
		$this->template->front($data);
	}
       
	
	function get_data_from_post()
	{
           
                        $data['name'] = $this->input->post('name', TRUE);
                        $data['email'] = $this->input->post('email', TRUE);
                        $data['subject'] = $this->input->post('subject', TRUE);
                        
                        $data['description'] = $this->input->post('description', TRUE);
                        
                         $data['status'] = 'live';
                        $data['slug'] = strtolower(url_title($data['name']));
		
			
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
                                
                                
			
		return $data;
           
           
	}
        function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		
		
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
                $this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');
                $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
                   
			$this->index(); 
                        
		}
		else
		{
                    //$expiration = time()-7200; // Two hour limit
                    //$this-> delete_captcha($expiration);
                    //$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	
			
			$data = $this->get_data_from_post();
			if($this->input->post('captcha', TRUE)==$this->input->post('word', TRUE)){
				$this->_insert($data);
   delete_files('./captcha/');
                        
			redirect('contact_us/details');
                        }
 else {
  $this->index();  
}
 
		}
			
	}
	
       function get($order_by){
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get($order_by);
	return $query;
	}
        function get_groups()
	{
	$this->load->model('department/mdl_department');
	$query = $this->mdl_department->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        
        function _insert($data){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_insert($data);
	}
        function delete_captcha($time){
            $this->load->model('mdl_contact_us');
	$this->mdl_contact_us->delete_captcha($data);
        }
}
