<script type="text/javascript">
</script>
                <h3>Contact Us</h3> 
                  
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
                                        
					echo form_open_multipart('contact_us/submit',  'class="form-horizontal row-border" id="validate-1"');				
				?>    
               <div class="col-lg-7">  
                
                	<div class="col-lg-12 margin-top-10">
                        
                          <?php echo form_input('name', 'Your Name', 'class="form-control required"');?>
                        
                    </div>  
                    <div class="col-lg-12 margin-top-10">
                        
                          <?php echo form_input('email', 'Your Email', 'class="form-control required"');?>
                        
                    </div> 
                    <div class="col-lg-12 margin-top-10">
                        
                          <?php echo form_input('subject', 'Subject', 'class="form-control required"');?>
                        
                    </div> 
                     
                    <div class="col-lg-12 margin-top-10">
                          <?php echo form_textarea(array('id' => 'elm1', 'name' =>'description', 'value' => 'Message here','rows'=>'15', 'cols'=>'100', 'style'=> 'width: 100%', 'class'=> 'form-control'));?>
                    </div> 
                    <div class="col-lg-12 margin-top-10">
                    	<?php echo $image;?>
                    </div>
                    <div class="col-lg-12 margin-top-10">
                    	<?php echo form_input('captcha', 'Enter Captcha', 'class="form-control required"');?>
                            <?php echo form_hidden('word', $word, 'class="form-control required"');?>
                    </div>
                     
                    <div class="col-lg-12 margin-top-10">
                        	<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"','id="submit"'); 
						?>
                    </div>
                
                <?php echo form_close(); ?>                
				</div>
                <div class="col-lg-5" style="padding-left:30px;">
                	<p style=" font-weight:bold; color:#bb0000; font-size:16px;">Honorary Consulate of The Republic of Kenya<br /></p>
                <p style="font-size:14px; color:#bb0000; font-weight:600;">Bellagio Office Park,<br />
                        OL2/30-32 Mega Kuningan<br />
                        Jakarta 12950 - Indonesia<br />
                        <strong>Phone: </strong>+62 21 30066151 - 53<br />
                        <strong> Fax  :</strong> +62 21 30066155/66<br />
                        <strong>Email:</strong> info@consulatekenya.org<br/>
                        <strong>Website: </strong>www.consulatekenya.org </p>
                        

                </div>           