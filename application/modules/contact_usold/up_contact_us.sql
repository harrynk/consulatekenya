-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2014 at 05:56 AM
-- Server version: 5.1.32
-- PHP Version: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upvedacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_contact_us`
--

CREATE TABLE IF NOT EXISTS `up_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_contact_us`
--

INSERT INTO `up_contact_us` (`id`, `name`, `email`, `subject`, `description`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Harry', 'me.harrynk@gmail.com', 'i am very sick', 'I am very sick please recommend me some medicine.\r\n', 'harry', '2014-07-16 00:00:00', '2014-07-16 00:00:00', 'live'),
(2, 'Dhruba kc', 'kcdhruba60@gmail.com', 'i am weak', 'i am feeling very week please recommend me some vitamine', 'dhruba-kc', '2014-07-16 00:00:00', NULL, 'live');
