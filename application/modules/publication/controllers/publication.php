<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publication extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_publication');
		parent::__construct();
	}


	function index(){
		$publication= $this->mdl_publication->get_front('id');
		$data['publication'] = $publication;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function publicationdetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}