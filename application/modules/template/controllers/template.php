<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function front($data){
		$data['site_settings'] = $this->get_site_settings();
		$data['banner'] = $this->get_banner();
//		$data['headernav'] = $this->get_header('1');	//this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise
                $data['headernav'] = $this->all_nav('1');
//		var_dump($data['headernav']); die;
//                foreach($data['headernav'] as $row)
//                    var_dump ($row);
//                die;
                $data['footernav'] = $this->get_footer('2');	//similar as above but 2 is for footer nav
		$data['news'] = $this->get_news_front();
                
                //var_dump($data['news']); die;
		$data['links'] = $this->get_links_front();
		$data['photos'] = $this->get_photos_front();
                $data['consulate_introduction'] = $this->get_consulate_introduction('consulate-office');
                $data['national_coat_of_arms'] = $this->get_national_coat_of_arms('national-coat-of-arms');
                $data['national_flag'] = $this->get_national_flag('national-flag');
                $data['national_anthem'] = $this->get_national_anthem('national-anthem');
                	
		$parent_id = $this->get_parentid();
		if($parent_id!=0){
			$data['sidebar'] = $this->get_child(1,$parent_id);
			$breadcrumb = $this->get_breadcrumb();
			$data['breadcrumb'] = $breadcrumb;
		}
		//print_r($data);
		$this->load->view('front', $data);
	}
	function all_nav($group_id){//to get navigation list of specific group
	$data['parentnav'] = $this->get_parent($group_id);	//
		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
                                $nav['children']= $children;
			}
		$navigation[$i] = $nav;
		$i++;	
		}
//                die;
		return $navigation;
                
	}
	function get_photos_front()
	{
		$this->load->model('photoscategory/mdl_photoscategory');
        $query = $this->mdl_photoscategory->get_front('id');
		//$result = $query->result();
		return $query;
	}
	
	function get_news_front()
	{
		$this->load->model('news/mdl_news');
		$query = $this->mdl_news->get_front('id');
		$result = $query->result();
		return $result;
	}
	
	function get_links_front()
	{
		$this->load->model('links/mdl_links');
		$query = $this->mdl_links->get_front('id');
		$result = $query->result();
		return $result;
	}
	
	function get_site_settings(){	
		$this->load->model('settings/mdl_settings');
		$query = $this->mdl_settings->get_settings();
		$result = $query->result_array();
		return $result[0];
	}	
	
	function get_navigation_from_navigation_name($navigation_name){		
		$this->load->model('navigation/mdl_navigation');
		$query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
		$result = $this->add_href($query->result_array());
		return $result;	
	}
	
	function errorpage(){
		$this->load->view('404');
	}
	
	function userlogin($data){
		$this->load->view('userlogin', $data);
	}
	
	function get_banner(){
	$this->load->model('banner/mdl_banner');
	$query = $this->mdl_banner->get_banner();
	return $query->result_array();
	}
	
	function get_parent($group_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_parentnav($group_id);	
	$result = $this->add_href($query);
	return $result;
	}
	function get_grandchild($group_id,$parent_id){
       
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_childnav($group_id,$parent_id);

	$result = $this->add_href($query);
        return $result;
        }
	function get_child($group_id,$parent_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_childnav($group_id,$parent_id);
//         foreach ($query as $sub_child)
//                                {
//                                    $grand_children =  $this->get_child('1',$sub_child['id']);
//                                    if($grand_children!=NULL)
//                                    {
//                                        $query['grand_children']=$grand_children;
//                                    }
//                                }
	$result = $this->add_href($query);
//        var_dump($result);die;
        $result = $this->add_hrefgrand($result);
//        var_dump($result);die;
                 
//       if(!empty($result['childe']))
//        if(!empty($result)){
//        foreach($result as $sub_child){
//         $grand_children =  $this->get_grand_child('1',$sub_child['id']);
//         if($grand_children != NULL){ 
//             
//            
//            }
//        }
        
//        }
        
	return $result;
	}
        function get_grand_child($group_id,$parent_id){
        $this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_childnav($group_id,$parent_id);
        $result = $this->add_href($query);
	return $result;
//	return $query;   
        }
        
	
	function get_header($group_id){
		$data['parentnav'] = $this->get_parent($group_id);	//
		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
				$nav['children'] = $children;
                                /*for grand child*/
                                if($nav['children'] == NULL){return NULL;}
                                $j=0;
                                
                                foreach($nav['children'] as $sub_child){
                                    $grand_children =  $this->get_grand_child($group_id,$sub_child['id']);
                                    
                                    if($grand_children != NULL){ 
				$nav['children']['grand_children'] = $grand_children;
                                   //var_dump($nav['children']['grand_children']); die();
                                    
                                }
                $navigation[$i][$j] = $nav;
		$j++;	
                               // var_dump($nav['children']); die();
			}
                        /*grand child end*/
			}
		$navigation[$i] = $nav;
		$i++;		
		}
		return $navigation;
	}
	
	function get_parentid()
	{
		$this->load->model('navigation/mdl_navigation');
		$parent_nav = $this->mdl_navigation->get_parentid();
		if(isset($parent_nav)&&($parent_nav!=NULL))
		{	
			$parent_id=$parent_nav[0]->parent_id;
		}
		else{
			$parent_id=0;
		}
		return $parent_id;
	}
	
	function get_breadcrumb()
	{
		$this->load->model('navigation/mdl_navigation');
		$parent_nav = $this->mdl_navigation->get_parentid();
		$parent_id=$parent_nav[0]->parent_id;
		$query = $this->mdl_navigation->get_where($parent_id);
		$result = $this->add_href($query);
		return $result;
	}
	
	/*function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);
	return $query->result_array();
	}*/
	
	function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);	
	$result = $this->add_href($query->result_array());
	return $result;
	}
	
	
	
	
	function add_href($result){
		$count = count($result);
		for($i=0;$i<$count;$i++){			
				if($result[$i]['navtype'] == 'Module'){$result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'Page'){$result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'URI'){$result[$i]['href'] = $result[$i]['site_uri'];$result[$i]['target'] = "_self";}
				
				else{$result[$i]['href'] = $result[$i]['link_url'];$result[$i]['target'] = "_blank";}			
		}
		return $result;
	}
	function add_hrefgrand($result){
		
            $i=0;
            if(isset($result)){
                    foreach ($result as $sub_child)
                                {
                                    $grand_children =  $this->get_grandchild('1',$sub_child['id']);
                                    if($grand_children!=NULL)
                                    {
                                        $result[$i]['grand_children']=$grand_children;
                                      
                                    }
                                   
                                    $i++;
                                }
            }
                                	
		return $result;
	}
	
	
	function get_name_from_module($module_id){		
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_module($module_id);	
	return $query['0']['slug'];	
	}
	
	function get_name_from_page($page_id){		
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_page($page_id);	
	return $query['0']['slug'];		
	}
         function get_consulate_introduction($slug){
            $this->load->model('pages/mdl_pages');
            $consulate_introduction = $this->mdl_pages->get_consulate_introduction($slug);
            return $consulate_introduction;
        }
        function get_national_coat_of_arms($slug){
            $this->load->model('pages/mdl_pages');
            $national_coat_of_arms = $this->mdl_pages->get_national_coat_of_arms($slug);
            return $national_coat_of_arms;
        }
        function get_national_flag($slug){
            $this->load->model('pages/mdl_pages');
            $national_flag = $this->mdl_pages->get_national_flag($slug);
            return $national_flag;
        }
        function get_national_anthem($slug){
            $this->load->model('pages/mdl_pages');
            $national_anthem = $this->mdl_pages->get_national_anthem($slug);
            return $national_anthem;
        }
}