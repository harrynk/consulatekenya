<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_template extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		//always check if session userdata value "logged_in" is not true
		if(!$this->session->userdata("logged_in"))/*every logged_in user get privilage to access dashboard*/
		{
			 redirect('admin');
		}
	
	}
	
	function admin($data){
			$user_id = $this->session->userdata['user_id'];
			$data['user_display_name'] = $this->get_username_from_user_id($user_id);
		
		$data['favicon'] = $this->get_attachment('favicon');
		$data['logo'] = $this->get_attachment('logo');		
			$group_id = $this->session->userdata['group_id'];//this usually represent what group the currently logged in user belong to
		$data['sidebar'] = $this->get_all_sidebar($group_id);
		$data['modules'] = $this->get_lists_of_modules('id');						// these three values are created and used in sidebar page of each...
		$data['pages'] = $this->get_lists_of_pages('id');							// ...module, page and navigation to display the sidbebar content... 
		$data['navigation'] = $this->get_lists_of_navigation_group('id');		// ...and use the loop from given variable to display results

		$this->load->view('admin/admin', $data);		
	}
	
	function get_all_sidebar($group_id){		
		if($group_id == '1') {$data['permissions'] = 'access_all';} //if admin we want it to access it all
		else { $data['permissions'] = $this->unserialize_role_array($group_id); } //if not we only want permissions what we are set to		
		
			if(empty($data['permissions'])){
				return NULL;}
			elseif($data['permissions'] == 'access_all')/*for admins*/{
				$modulelists = $this->get_all_slug_from_module();
				return $modulelists;
				}
			else
			{/*if there is an array*/
				$i = 1;
				foreach($data['permissions'] as $module_id){
					$modulelists[$i] = $this->get_slug_from_moduleid($module_id);	
					$i++;
				}
				return $modulelists;
			}
						
	}
	
	function get_username_from_user_id($user_id){	
	$this->load->model('users/mdl_users');
	$username = $this->mdl_users->get_username_from_user_id($user_id);
	return $username;			
	}
	
	function get_all_slug_from_module(){		
	$this->load->model('modules/mdl_moduleslist');
	$module_array = $this->mdl_moduleslist->get_all_slug_from_module();
	return $module_array;	
	}
	
	function get_slug_from_moduleid($module_id){		
	$this->load->model('modules/mdl_moduleslist');
	$module_name = $this->mdl_moduleslist->get_slug_from_moduleid($module_id);
	return $module_name;	
	}
	
	
	function unserialize_role_array($group_id){		
	$this->load->model('permissions/mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
		
	function get_attachment($name){		
		$this->load->model('settings/mdl_settings');
		$query = $this->mdl_settings->get_where($name);
		$result = $query->result_array();
		return $result[0][$name];
	}
		
	function get_lists_of_modules($order_by){		
		$this->load->model('modules/mdl_moduleslist');
		$query = $this->mdl_moduleslist->get($order_by);
		return $query->result_array();
	}
		
	function get_lists_of_pages($order_by){		
		$this->load->model('pages/mdl_pages');
		$query = $this->mdl_pages->get($order_by);
		return $query->result_array();
	}
		
	/*function get_lists_of_navigation($order_by){		
		$this->load->model('navigation/mdl_navigation');
		$query = $this->mdl_navigation->get_ind_nav('header');//wrote 'header' because here we want to access header nav group
		return $query->result_array();
	}*/
			
	function get_lists_of_navigation_group($order_by){		
		$this->load->model('navigation/mdl_navigation_group');
		$query = $this->mdl_navigation_group->get_group($order_by)->result();
		return $query;
	}
	
}