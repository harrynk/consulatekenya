<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $site_settings['site_name']; ?></title>
        <meta name="title" content="<?php echo $site_settings['site_name']; ?>">
        <meta name="keywords" content="<?php echo $site_settings['meta_topic']; ?>">
        <meta name="description" content="<?php echo $site_settings['meta_data']; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['favicon']; ?>">

        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/responsive-slider.css">
        <!--<link href="<?php echo base_url(); ?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>  
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css">  -->
        <!--social icon-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-social.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-social.less">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/font-awesome.css">
        <!--social icon end-->

        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/bootstrap.min.js"></script>

        <!-- For Responsive Photo Gallery-->
        <script src="<?php echo base_url(); ?>design/frontend/js/gallery/jquery.blueimp-gallery.min.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/gallery/bootstrap-image-gallery.min.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/gallery/demo.js"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/gallery/blueimp-gallery.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/gallery/bootstrap-image-gallery.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/gallery/demo.css">

    </head>
    <body>
        <div class="container">    
            <div id="heading_top">
                <div class="row">
                    <div class="col-lg-2"> 
                        <a title="Honorary Consulate of the Republic of Kenya in Indoensia" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['logo']; ?>" alt="Honorary Consulate of the Sloval Republic in Pakistan"  height="80" style="text-align:center;"></a>
                        <?php
                        //$post_data=$this->input->post();
                        //echo $post_data;
                        ?>
                    </div>
                    <div class="col-lg-3">	
                        <div class="logo_info">
                            HONORARY CONSULATE OF THE 
                        </div>
                        <div class="logo_info_title">
                            REPUBLIC OF KENYA
                        </div>
                        <div class="logo_info">
                            IN INDONESIA
                        </div>

                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                            <!--                    <ul class="top_right_nav">
                                                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                                    <li><a href="<?php echo base_url(); ?>consulate-introduction">About Us</a></li>
                                                    <li><a href="<?php echo base_url(); ?>contact_us">Contact Us</a></li>
                                                </ul>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!--navigation bar------------------------------------------------------------>

            <div class="navbar navbar-default menu_background" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>



                <div class="navbar-collapse collapse" style="height: 1px;">
                    <ul class="nav navbar-nav">

                        <?php foreach ($headernav as $header) {//still need to work on slug?> 
                            <?php
                            if (empty($header['children'])) {
                                echo '<li>'; /* this is for no-child */
                            } else {
                                ?>                    	
                                <li class="dropdown">
                                <?php }/* this is for dropdown */ ?>

                                <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                    <?php
                                    if ($header['navtype'] == 'URL') {
                                        $prefix = '';
                                    } else {
                                        $prefix = base_url();
                                    }
                                    ?>
                                    <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?>                                   
                                    <?php }/* end of if */ else { /* this is for no-child */
                                        ?>                            
                                        <?php
                                        if ($header['navtype'] == 'URL') {
                                            $prefix = '';
                                        } else {
                                            $prefix = base_url();
                                        }
                                        ?>
                                        <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
                                            <?php } ?>                           
                                    </a>

                                            <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                        <ul class="dropdown-menu">
                                                <?php foreach ($header['children'] as $child) { ?>  
                                                <li>                                            
                                                <?php
                                                if ($child['navtype'] == 'URL') {
                                                    $prefix = '';
                                                } else {
                                                    $prefix = base_url();
                                                }
                                                ?>
                                                    <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                                </li>
        <?php }/* end of child foreach */ ?>
                                        </ul>
                <?php } ?>  

                            </li>
            <?php }/* end of parent foreach */ ?>

                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--banner-------------------------------------------------------------------->

<?php
$first_bit = $this->uri->segment(1);
if ($first_bit == "" || $first_bit == "home") {
    ?>


                <!--============================================start of responsive slider============================-->

                <div class="col-lg-12" style="margin-bottom:10px;">
                    <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                        <div class="slides" data-group="slides">
                            <ul>
    <?php foreach ($banner as $row) { ?> 
                                    <li>
                                        <div class="col-lg-4 responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                            <div class="slide-body slider_color" data-group="slide">
                                                <h4><strong><?php echo ucwords($row['title']) ?></strong></h4>
                                                <p> <?php echo word_limiter($row['sub_title'], 90); ?></p>
        <!--                                                                <a href="<?php echo base_url() ?>banner/index/<?php echo $row['slug']; ?>" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-8 responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                            <div class="slide-body" data-group="slide">
                                                <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>" style="width:1140px; height:300px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
    <?php } ?>
                            </ul>
                        </div>
                        <div class="pages">
                            <a class="page" href="#" data-jump-to="1">1</a>
                            <a class="page" href="#" data-jump-to="2">2</a>
                            <a class="page" href="#" data-jump-to="3">3</a>
                            <a class="page" href="#" data-jump-to="4">4</a>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>       

                <script src="<?php echo base_url(); ?>design/public/js/responsive-slider.js"></script>
                <script src="<?php echo base_url(); ?>design/public/js/jquery.event.move.js"></script>

                <!--==============================================end of responsive slider============================-->   

                    <?php } ?>



            <!-- start of the content ------------------------------------------>
            <div id="content">
                <div class="row">
                            <?php
                            $first_bit = $this->uri->segment(1);
                            if ($first_bit == "" || $first_bit == "home") {
                                ?>
                        <div class="col-lg-8">
                            <div class="support">
                                <h4>The Consulate</h4>
    <?php foreach ($consulate_introduction as $row) { ?>
        <?php echo word_limiter($row['description'], 50); ?>
    <?php } ?>

        <!--                            	<center><img src="<?php echo base_url(); ?>design/frontend/img/kenya_map.png" class="img-responsive" /></center>
           <p> <strong>Welcome</strong> to the website of the Honorary Consulate of the Republic in Kenya.</p>-->

                                <span  style="margin-top:20px;"><a href="<?php echo base_url() . 'consulate-office'; ?>" class="read_more">Read more</a></span>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="support">
                                <h4>News and events</h4>
                                <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/initcarousel-1.css"><!--
                                -->                        <script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/amazingcarousel.js"></script><!--
                                -->                        <script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/amazingcarousel/initcarousel-1.js"></script>

                                <div id="amazingcarousel-container-1">
                                    <div id="amazingcarousel-1" style="display:block;position:relative;width:100%;max-width:600px;margin:0px auto 0px;">
                                        <div class="amazingcarousel-list-container">
                                            <ul class="amazingcarousel-list">
    <?php foreach ($news as $singnews) { //var_dump($singnews);  ?>
                                                    <li class="amazingcarousel-item">
                                                        <div class="amazingcarousel-item-container">
                                                            <div class="amazingcarousel-image"><img src="<?php echo base_url(); ?>uploads/news/<?php echo $singnews->attachment; ?>"  alt="" /></div>
                                                            <div class="amazingcarousel-text">
                                                                <div class="amazingcarousel-title"><a href="<?php echo base_url() . 'news/details/' . $singnews->slug; ?>"><?php echo $singnews->title; ?></a></div>
        <!--                                                            <div class="amazingcarousel-description"><?php echo substr($singnews->description, 0, 30) . '...'; ?></div>-->
                                                                <div class="amazingcarousel-description"><?php echo $singnews->description; ?></div>
                                                            </div>
                                                            <div style="clear:both;"></div>         
                                                        </div>
                                                    </li>
    <?php } ?>
                                            </ul>
                                            <div class="amazingcarousel-prev"></div>
                                            <div class="amazingcarousel-next"></div>
                                        </div>
                                        <div class="amazingcarousel-nav"></div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <?php
//								foreach($news as $singnews)
//								{
                                ?>
        <!--                            <h5><?php echo $singnews->title; ?></h5>
                                    <p><?php echo substr($singnews->description, 0, 400) . '...'; ?> 
                                    <span ><a href="<?php echo base_url() . 'news/details/' . $singnews->slug; ?>" class="read_more">Read more</a></span>
                                    </p>
                                    <hr />-->
    <?php
    //}
    ?>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="support">
                                <h4>Quick Links</h4>
                                <div class="link">
                                    <!--                        <ul class="list-group">
    <?php
    foreach ($links as $singlinks) {
        ?>
                                                                                    <li class="list-group-item"> <a href="<?= $singlinks->links; ?>" target="_blank" ><?= $singlinks->title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                                              
                                                             </ul>-->
                                    <ul class="sidenav">
    <?php
    foreach ($links as $singlinks) {
        ?>
                                            <li> <a href="<?= $singlinks->links; ?>" target="_blank" ><?= $singlinks->title; ?></a></li>
        <?php
    }
    ?>

                                    </ul>
                                </div>
                            </div>
                            <div class="support" style="min-height: 296px;">
                                <div class="gallery">
                                    <h4> Gallery</h4>
                                    <div class="row">
    <?php
    foreach ($photos as $singphotos) {
        ?>
                                            <a href="<?php echo base_url() ?>photos/details/<?php echo $singphotos->slug; ?>"><img src="<?php echo base_url(); ?>uploads/photos/<?php echo $singphotos->attachment; ?>" width="48%" style="margin-top:5px; border:#666666 1px solid;"/></a>
        <?php
    }
    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="support support_padding">
                                <a class="placestovisitlink" href="<?php echo base_url() . 'national-coat-of-arms'; ?>"><img src="<?php echo base_url(); ?>design/frontend/img/coat-of-arms.png" class="well" style="width:20%;" alt="" /><strong>National Coat of Arms</strong></a>
                                <a class="whattoseelink" href="<?php echo base_url() . 'national-flag'; ?>"><img src="<?php echo base_url(); ?>design/frontend/img/Mt_Kenya_Flag.jpg"  class="well"  style="width:20%;" alt="" /><strong>National Flag</strong></a>
                                <a class="whattodolink" href="<?php echo base_url() . 'national-anthem'; ?>"><img src="<?php echo base_url(); ?>design/frontend/img/kenya_map.png"  class="well"  style="width:20%;" alt="" /><strong>National Anthem</strong></a>
                                <audio controls>
                                    <source src="<?php echo base_url(); ?>design/frontend/img/National_Anthem_of_Kenya.ogg" type="audio/ogg">
                                </audio>
                            </div>
                        </div>  
    <?php
} else {
    ?>
                        <div class="col-lg-12">
                            <div class="breadcrumbs_div">  
                                <ul id="breadcrumbs" class="breadcrumb">  
                                    <li>  
                                        <i class="icon-home"></i>  
                                        <a href="<?php echo base_url(); ?>">Home</a> 
                                    </li>  
                                    <li> 
                                        <?php
                                        if (isset($breadcrumb)) {
                                            foreach ($breadcrumb as $bread) {
                                                if ($bread['navtype'] == 'URL') {
                                                    $prefix = '';
                                                } else {
                                                    $prefix = base_url();
                                                }
                                                ?>
                                                <a href="<?php echo $prefix . $bread['href'] ?>" target="<?php echo $bread['target'] ?>"><?php echo $bread['title'] ?></a>                              
                                            <?php
                                        }
                                        ?>
                                        <li> 
                                        <?php echo $title; ?>
                                        </li>
                                        <?php
                                    } else {
                                        $module = $this->uri->segment(1);
                                        if (isset($title)) {
                                            echo $title;
                                        } else {
                                            echo $module;
                                        }
                                    }
                                    ?>
                                    </li>
                                </ul>  
                            </div>

                        </div>
                        <div class="clearfix"></div>
                                <?php
                                if (isset($sidebar)) {
                                    ?>
                            <div class="col-lg-3 background-fcfcfc">
                                <ul class="sidenav">
                                    <?php
                                    foreach ($sidebar as $header) {
                                        ?>
                                        <?php
                                        if ($header['navtype'] == 'URL') {
                                            $prefix = '';
                                        } else {
                                            $prefix = base_url();
                                        }
                                        ?>
                                        <li><a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?></a></li>                              
                                    <?php
                                }
                                ?>
                                </ul>
                            </div>
                                <?php
                            }
                            ?>

                        <div class="col-lg-<?php echo (isset($sidebar) ? '9' : '12'); ?> main-body">            
                            <!--                            <h3>
                            <?php
//                                        if($this->uri->segment(3))
//                                        {
//                                            $title_new=$this->uri->segment(3);
//                                            $title =urldecode($title_new);
//                                        }
//                                        else
//                                        {
//                                            $title = $this->uri->segment(1);
//                                        }
//                                            if(strpos($title,'_') !=false)
//                                            {
//                                            $title_array=explode('_',$title);
//                                            }
//                                            else
//                                            {
//                                            $title_array=explode('-',$title);
//                                            }
//
//                                            foreach($title_array as $tarray)
//                                            {
//                                            echo ucwords(strtolower($tarray));
//                                            echo ' ';
//                                            }
                            //   echo $title_echo;
                            ?>
                                                        </h3>-->
                            <?php
                            if (!isset($view_file)) {
                                $view_file = "";
                            }
                            if (!isset($module)) {
                                $module = $this->uri->segment(1);
                            }
                            if (($view_file != '') && ($module != '')) {
                                $path = $module . "/" . $view_file;
                                $this->load->view($path);
                            } else {
                                echo '<h3>' . $title . '</h3>';
                                $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                echo $new_description; //put your designs here
                                $slug = $this->uri->segment(1);
                                if ($slug == 'government') {
                                    ?>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8171104.233693699!2d33.40710474785517!3d0.1873567148173795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182780d08350900f%3A0x403b0eb0a1976dd9!2sKenya!5e0!3m2!1sen!2snp!4v1459339237466" width="730" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    <small>
                                        <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8171104.233693699!2d33.40710474785517!3d0.1873567148173795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182780d08350900f%3A0x403b0eb0a1976dd9!2sKenya!5e0!3m2!1sen!2snp!4v1459339237466"></small>
                                <?php
                            }
                            if ($slug == 'investment-opportunities') {
                                ?>
                                    <div class="col-md-6">
                                    <a title="Investment Opportunities" href="http://www.investmentkenya.com" target="_blank"><img src="<?php echo base_url(); ?>uploads/settings/investment_opportunity.png" alt="Investment Opportunity"  style="text-align:center; width: 300px;"></a>
                                    </div>
                                    <div class="col-md-6 investmentkenya">
                                        <h2>Investmentkenya<h2>
                                    </div>
            <?php
        }
    }
    ?>
                        </div>

<?php }
?>
                </div>
                <div class="footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sub-footer">
                                <div class="col-lg-3" style="padding:0px 10px;">
                                    <h3>Contact Us</h3>
                                    <hr class="hr-style">
                                    <ul style="margin-left: -35px;">
                                        <li>THE HONORARY CONSULATE OF THE REPUBLIC OF KENYA IN INDONESIA</li>
                                        <li>Phone: 2954 2704</li>
                                        <li>Fax: 2954 2705</li>
                                        <li>Mail: info@consulatekenya.org,
                                            becky@consulatekenyaid.org</li>
                                    </ul>
                                </div>
                                <div class="col-lg-3" style="padding:0px 10px; color: white;">
                                    <h3>About Us</h3>
                                    <hr class="hr-style">
                                    <ul class="sidenav" style="margin-left: -35px; padding-left: 30px;background-color:#060; border:none;">
<?php foreach ($headernav[0]['children'] as $footer) {//still need to work on slug   ?>
                                            <p style="font-weight: 700;
                                               font-size: 14px;">                                            
                                                <a style="color: white;" href="<?php echo $footer['href']; ?>" target="<?php echo $footer['target'] ?>"><?php echo $footer['title']; ?></a>
                                            </p>
<?php } ?>
                                    </ul>
                                </div>
                                <div class="col-lg-3" style="padding:0px 10px;">
                                    <div>
                                        <h3>Find Us</h3>
                                        <hr class="hr-style">
                                        <div class="icon-social social">
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                                            </ul>
                                        </div>  
                                    </div>
                                    <div class="clearfix"></div>
                                    <div>
                                        <h3>Translation</h3>
                                        <hr class="hr-style">
                                        <select>
                                            <option value="volvo">English</option>
                                            <option value="saab">Nepali</option>
                                            <option value="opel">Hindi</option>
                                            <option value="audi">Chinese</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3" style="padding:0px 10px;">
                                    <h3>About Us</h3>
                                    <hr class="hr-style">
                                    <p>Welcome to the official website of the Honorary Consulate of the Republic of Kenya in Indonesia. This site has information on tourism, investment opportunities, and consular services and more. Kenya, is the cradle of mankind and a sub-Sahara Africa investment and air transportation hub; the Honorary Consul Mr. Bilal Asif is honored to invite all who may have an interest on Kenya to contact the office for further assistance. We are committed to excel in service and welcome all complaints and compliments.</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12" style="border-top: 2px solid #fff;">
                                <p style="text-align:left; color:#ffffff; padding-left:20px;margin-top: 13px !important;">© 2016 The Honorary Consulate of The Republic of Kenya in Indonesia</p>
                            </div>
                            <!--                    <div class="col-lg-3">
                                                    <div class="footernav">
                                                        <ul class="nav">
<?php foreach ($footernav as $footer) {//still need to work on slug  ?> 
                                                                        <li>
    <?php
    if ($footer['navtype'] == 'URL') {
        $prefix = '';
    } else {
        $prefix = base_url();
    }
    ?>
                                                                            <a href="<?php echo $prefix . $footer['href']; ?>" target="<?php echo $footer['target']; ?>"><?php echo $footer['title']; ?></a>
                                                                        </li>
<?php } ?>
                                                        </ul>
                                                      </div>
                                                </div>-->
                        </div>
                    </div>
                </div>
            </div><!---end-content--->


        </div><!--end of container-->
        <script src="<?php echo base_url(); ?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url(); ?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/responsive-slider.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X');
            ga('send', 'pageview');
        </script>


    </body>
</html>