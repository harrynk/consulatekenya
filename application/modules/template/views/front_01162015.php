<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $site_settings['site_name'];?></title>
<meta name="title" content="<?php echo $site_settings['site_name'];?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic'];?>">
<meta name="description" content="<?php echo $site_settings['meta_data'];?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">

<script src="<?php echo base_url();?>design/frontend/js/jquery.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>

<!-- For Responsive Photo Gallery-->
<script src="<?php echo base_url();?>design/frontend/js/gallery/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery/bootstrap-image-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery/demo.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/bootstrap-image-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/demo.css">

</head>
<body>
    <div class="container">    
        <div id="heading_top">
          <div class="row">
            <div class="col-lg-1"> 
                <a title="Honorary Consulate of the Sloval Republic in Pakistan" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['logo'];?>" alt="Honorary Consulate of the Sloval Republic in Pakistan"  height="80" style="text-align:center;"></a>
                <?php 
				//$post_data=$this->input->post();
					//echo $post_data;
				?>
             </div>
            <div class="col-lg-3">	
                <div class="logo_info">
                	HONORARY CONSULATE OF THE 
                </div>
                <div class="logo_info_title">
                	SLOVAK REPUBLIC
                </div>
                <div class="logo_info">
                	IN PAKISTAN
                </div>
                
            </div>
            <div class="col-lg-8">
            	<div class="col-lg-7"></div>
                <div class="col-lg-5">
                    <ul class="top_right_nav">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="<?php echo base_url();?>consulate-introduction">About Us</a></li>
                        <li><a href="<?php echo base_url();?>contact_us">Contact Us</a></li>
                    </ul>
                </div>
            </div>
          </div>
        </div>
    	<div class="clearfix"></div>
    	<!--navigation bar------------------------------------------------------------>
        
        <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                

                
                <div class="navbar-collapse collapse" style="height: 1px;">
                  <ul class="nav navbar-nav">
                  
                    <?php foreach($headernav as $header){//still need to work on slug?> 
                    <?php if(empty($header['children'])){ echo '<li>';/*this is for no-child*/} else{?>                    	
                    <li class="dropdown">
                    <?php }/*this is for dropdown*/?>
                    
							<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title']?>                                   
                            <?php }/*end of if*/
                            else { /*this is for no-child*/?>                            
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?>
                            <?php } ?>                           
                                </a>
                                    
									<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                                    <ul class="dropdown-menu">
                                        <?php foreach($header['children'] as $child){ ?>  
                                            <li>                                            
                            					<?php if($child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                                <a href="<?php echo $prefix.$child['href']; ?>" target="<?php echo $child['target']?>"><?php echo $child['title']; ?></a>
                                            </li>
                                        <?php }/*end of child foreach*/ ?>
                                    </ul>
                                    <?php } ?>  
                        
                    </li>
                    <?php }/*end of parent foreach*/ ?>
                    
                  </ul>
                </div><!--/.nav-collapse -->
              </div><!--banner-------------------------------------------------------------------->
              
    				<?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
									
								
				<!--============================================start of responsive slider============================-->
					
						<div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
					<div class="slides" data-group="slides">
						<ul>
							<?php foreach($banner as $row){ //displaying all banner ?> 
								<li>
								  <div class="slide-body" data-group="slide">
									<img src="<?php echo base_url();?>uploads/banner/<?php echo $row['attachment']?>" style="width:1140px;">
                                    <div class="caption header" data-animate="slideAppearLeftToRight" data-delay="500" data-length="300" style="opacity: 1; margin-left: 0px; margin-right: 0px;">
                  						<h2>In the Heart of Europe</h2>
                					</div>
                                    
								  </div>
								</li>
							<?php }?>
						</ul>
					</div>
					<!-- <a class="slider-control left" href="#" data-jump="prev"><</a>
					<a class="slider-control right" href="#" data-jump="next">></a> -->
					
			   </div>
			  
				<script src="<?php echo base_url();?>design/public/js/responsive-slider.js"></script>
			<script src="<?php echo base_url();?>design/public/js/jquery.event.move.js"></script>
			 
			  <!--==============================================end of responsive slider============================-->   
				
					<?php }?>
                    
					             
    		
		<!-- start of the content ------------------------------------------>
        <div id="content">
            <div class="row">
        	<?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
                    <div class="col-lg-4">
                    	<div class="support">
                        	<h4>Recent News</h4>
                            <?php
								foreach($news as $singnews)
								{
							?>
                            <h5><?php echo $singnews->title; ?></h5>
                            <p><?php echo substr($singnews->description,0,400).'...'; ?> 
                            <span class="read_more"><a href="<?php echo base_url().'news/details/'.$singnews->slug;?>">Read more</a></span>
                            </p>
                            <hr />
                            <?php
								}
							?>
                        </div>
                    </div>
                    <div class="col-lg-4">
                    	<div class="support">
                        	<h4>The Consulate</h4>
                            <p>
                            	<img src="http://www.upvedatech.com/upvedacms/uploads/default/Image/slovakia_flag_map.png" class="img-responsive" /><b>Welcome</b> to the website of the Honorary Consulate of the Slovak Republic in Lahore, Pakistan. Miroslav Lajčák, Honorable Deputy Prime Minister and Minister of Foreign and European affairs of the Slovak Republic, ceremonially opened the Honorary Consulate of Slovakia in Lahore (Punjab Province) and appointed Malik Muhammad Asif to the office of the Honorary Consul today (10 June 2014) on the second day of his official visit to Pakistan.</p>
                             
                            <span class="read_more" style="margin-top:20px;"><a href="<?php echo base_url().'consulate-introduction'; ?>">Read more</a></span>
                            </p>
                        </div>
                    </div> 
                    <div class="col-lg-4">
                      <div class="support">
                        <h4>Quick Links</h4>
                        <div class="link">
                        <ul class="list-group">
                        	<?php
								foreach($links as $singlinks)
								{
							?>
                          		<li class="list-group-item"> <a href="<?= $singlinks->links; ?>" target="_blank"><?= $singlinks->title; ?></a></li>
                          	<?php
								}
							?>
                          
                         </ul>
                        </div>
                      </div>
                      <div class="support">
                        <div class="gallery">
                          <h4> Gallery</h4>
                          <div class="row">
                          <?php
								foreach($photos as $singphotos)
								{
							?>
								<a href="<?php echo base_url()?>photos/details/<?php echo $singphotos->slug;?>"><img src="<?php echo base_url();?>uploads/photos/<?php echo $singphotos->attachment; ?>" width="48%" style="margin-top:5px; border:#666666 1px solid;"/></a>
                          <?php
								}
						  ?>
                          </div>
                        </div>
                      </div>
                    </div>  
                    <?php
					}
					else
					{
					?>
                    		<div class="col-lg-12">
                            	<div class="col-lg-8 breadcrumbs">
                                	<a href="<?php echo base_url();?>">Home</a> > 
                                    <?php
									if(isset($breadcrumb))
									{
										foreach($breadcrumb as $bread)
										{
											if($bread['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                            <a href="<?php echo $prefix.$bread['href']?>" target="<?php echo $bread['target']?>"><?php echo $bread['title']?></a>                              
                                        <?php 
										}
										echo ' > '. $title;
									}
									else{
										$module = $this->uri->segment(1);
										if(isset($title))
										{
										echo $title;
										}else{
											echo $module;
										}
									}
									?>
                                    
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
								if(isset($sidebar))
								{
							?>
                    		<div class="col-lg-3 background-fcfcfc">
                            	<ul class="sidenav">
                            	<?php
									foreach($sidebar as $header){
								?>
										<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                            <li><a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?></a></li>                              
                                        <?php 
                                        
                                	}

								?>
                                </ul>
                            </div>
                            <?php
								}
							?>
        
                            <div class="col-lg-<?php echo (isset($sidebar)?'9':'12'); ?> main-body">            
                            <?php
                                if(!isset($view_file)){
                                    $view_file="";
                                }
                                if(!isset($module)){
                                    $module = $this->uri->segment(1);
                                }
                                if(($view_file!='') && ($module!='')){
                                    $path = $module."/".$view_file;
                                    $this->load->view($path);
                                } 
                                else {
									echo '<h3>'.$title.'</h3>';
                                    $new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
                                    echo $new_description;//put your designs here
                                }
                                
                            ?>
                            </div>
                            
                    <?php
					}?>
          </div>
          <div class="footer_top">
            	<div class="col-lg-12">
                	<div class="img_link">
          				<a href="<?php echo base_url()?>uploads/default/flag.png"><img src="<?php echo base_url()?>design/frontend/img/flag_link.png" /></a>
          			</div>
                    <div class="img_link">
          				<a href="<?php echo base_url()?>uploads/default/national_anthem.mp3"><img src="<?php echo base_url()?>design/frontend/img/national_anthem_link.png" /></a>
          			</div>
                    <div class="img_link">
          				<a href="<?php echo base_url()?>uploads/default/coat_of_arms.png"><img src="<?php echo base_url()?>design/frontend/img/coat_of_arms_link.png" /></a>
          			</div>
                    <div class="img_link">
          				<a href="<?php echo base_url()?>uploads/default/big_coat_of_arms.png"><img src="<?php echo base_url()?>design/frontend/img/big_coat_of_arms_link.png" /></a>
          			</div>
                </div>
          </div>
          <div class="footer">
            <div class="row">
              <div class="col-lg-12">
                    <div class="col-lg-9" style="text-align:left; color:#993366; padding-left:20px; margin-top:10px;">
                    	© 2014 The Honorary Consulate of the Slovak Republic ·57/3 Sher Khan Road,Cantt, Lahore, Pakistan
                    </div>
                    <div class="col-lg-3">
                    	<div class="footernav">
                            <ul class="nav">
                                <?php foreach($footernav as $footer){//still need to work on slug?> 
                                    <li>
                                        <?php if($footer['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                        <a href="<?php echo $prefix.$footer['href'];?>" target="<?php echo $footer['target'];?>"><?php echo $footer['title']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                          </div>
                    </div>
              </div>
            </div>
          </div>
        </div><!---end-content--->
        
        
	</div><!--end of container-->
		<script src="<?php echo base_url();?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url();?>design/frontend/js/responsive-slider.js"></script>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    
    
</body>
</html>