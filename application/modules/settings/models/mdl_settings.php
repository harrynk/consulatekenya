<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_settings extends CI_Model 
{
	
	function __construct() {
	parent::__construct();
	}	
	
	function get_table() {
	$table = "up_settings";
	return $table;
	}
	
	function get_settings(){
	$table = $this->get_table();
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($name){
	$table = $this->get_table();
	$this->db->select($name);
	$this->db->where('id', 1);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}	
	
	
}