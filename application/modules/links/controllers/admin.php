<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('links'); //module name is links here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['title'] = $this->input->post('title', TRUE);
                        $data['links'] = $this->input->post('links', TRUE);
                        $data['status'] = $this->input->post('status', TRUE);
                        $data['slug'] = strtolower(url_title($data['title']));
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = date("Y-m-d");;
			}
			
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where1($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
			$data['links'] = $row->links;
			$data['status'] = $row->status;	
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_links');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/links');
			}
		else
		{
			$this->mdl_links->_delete($delete_id);
			redirect('admin/links');
		}				
	}
	
	
	function submit()
	{		
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('title', 'Title', 'required|xss_clean'); //we don't want unique_validation error while editing
                      
		}
		else{
		$this->form_validation->set_rules('title', 'Title', 'required|xss_clean|is_unique[up_links.name]'); //unique_validation check while creating new
		
                
                }
		/*end of validation rule*/	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){						
				$this->_update($update_id, $data);
			} else {								
					
				$nextid = $this->get_id();				
				$this->_insert($data);	
			}
		
		redirect('admin/links');	
			
	}
	
		

	function get_id(){
	$this->load->model('mdl_links');
	$id = $this->mdl_links->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_links');
	$query = $this->mdl_links->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_links');
	$query = $this->mdl_links->get_where($id);
	return $query;
	}
	function get_where1($id){
	$this->load->model('mdl_links');
	$query = $this->mdl_links->get_where1($id);
	return $query;
	}
	function _insert($data){
	$this->load->model('mdl_links');
	$this->mdl_links->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_links');
	$this->mdl_links->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_links');
	$this->mdl_links->_delete($id);
	}
}