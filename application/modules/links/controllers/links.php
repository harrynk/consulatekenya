<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news extends MX_Controller
{

	function __construct() {
            
		$this->load->model('mdl_news');
		parent::__construct();
	}

function index(){
            
		$news = $this->get('id');
                $data['news']=$news;
		$data['view_file']="table";
		$this->load->module('template');
		$this->template->front($data);
	}
	
function details()
	{ 
		$slug = $this->uri->segment(3);
                
		$query= $this->get_where($slug);
                foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
                        $data['description'] = $row->description;
                       $data['published_date'] = $row->published_date;
		}
                
                
               
		$data['view_file']="form";
		$this->load->module('template');
		$this->template->front($data);
	}
         function get_where($id){
	
	$query = $this->mdl_news->get_where($id);
	return $query;
	}
       function get($order_by){
	$this->load->model('mdl_news');
	$query = $this->mdl_news->get($order_by);
	return $query;
	}
}
