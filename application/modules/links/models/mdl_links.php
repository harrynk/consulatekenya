<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_links extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_links";
	return $table;
	} 
        
    function get_front($order_by){
	$table = $this->get_table();
    $this->db->where('status', 'live');
	$this->db->order_by($order_by,'DESC');
	$query=$this->db->get($table);
	return $query;
	}
        
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('slug', $id);
	$query=$this->db->get($table);
	return $query;
      
	}
        function get_where1($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
      
	}
	
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function get_id(){
	$links = mysql_query("SHOW TABLE STATUS LIKE 'up_links'");
	$row = mysql_fetch_array($links);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_links')->links();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
                $this->db->where('status', 'live');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_links')->links();
                $dropdownlist[0] = '';//for making parent value null as default
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}   

}