-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2014 at 04:21 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `slovakconsulate`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_links`
--

CREATE TABLE IF NOT EXISTS `up_links` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `links` varchar(255) NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime NOT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_links`
--

INSERT INTO `up_links` (`id`, `title`, `slug`, `links`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Facebook', 'facebook', 'http://www.facebook.com', '2014-08-05 00:00:00', '2014-08-05 00:00:00', 'live'),
(2, 'Gmail', 'gmail', 'http://www.gmail.com', '2014-08-05 00:00:00', '2014-08-05 00:00:00', 'live'),
(3, 'Hotmail', 'hotmail', 'http://www.hotmail.com', '2014-08-05 00:00:00', '2014-08-05 00:00:00', 'live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
