<?php    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('contact_us'); //module name is groups here	
	}
	
	function view()
        {
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
		$view_id = $this->uri->segment(4);
		if($group_id != 1){
				if (is_numeric($view_id))
                                    {
                                        $permissions = $this->unserialize_role_array($group_id);
                                        $modulename = $this->uri->segment(2);
                                        $module_id=$this->get_id_from_modulename($modulename);
                                        $mystring = implode(" ",$permissions);
                                        
                                        if((strpos($mystring, 'v'.$module_id))==false)
                                        {
                                            redirect('admin/dashboard');  
                                        }
                                        else
                                        {
                                         $data = $this->get_data_from_db($view_id);   
                                        }
                                      }
                                    }
                        $data = $this->get_data_from_db($view_id);
			$data['view_id'] = $view_id;
			$data['view_file'] = "admin/view";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
                }	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	 
	function get_data_from_post()
	{
		
		$data['name'] = $this->input->post('name', TRUE);
                $data['phone_no'] = $this->input->post('phhone_no', TRUE);
                $data['country'] = $this->input->post('country', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['subject'] = $this->input->post('subject', TRUE);
                $data['description'] = $this->input->post('description', TRUE);
                $data['address'] = $this->input->post('address', TRUE);
                $data['slug'] = strtolower(url_title($data['name']));	
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d H:i:s");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d H:i:s");
				//$data['upd_date'] = NULL;
			}
		
		
			
			
		return $data;
	}
		
		
	function get_data_from_db($update_id)
	{      
		$query = $this->get_where($update_id);
		foreach($query as $row)
		{			
			
			
			$data['name']= $row->name;
                        $data['phone_no']= $row->phone_no;
                        $data['country']= $row->country;
                        $data['email']= $row->email;
                        $data['subject']= $row->subject;
                        $data['description']= $row->description;
                        $data['address']= $row->address;
                        $data['slug']= $row->name;
			
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		$submit = $this->input->post('submit', TRUE);
	
		if($submit=="Submit"){
			//person has submitted the form
			$data = $this->get_data_from_post();
		} 
		else {
			if (is_numeric($update_id)){
				$data = $this->get_data_from_db($update_id);
			}
		}
		
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
		
		$data['update_id'] = $update_id;
	//	$data['name_array'] = $this->get_membercategory();////////////////
				
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_contact_us');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/contact_us');
			}
		else
		{
			$this->mdl_contact_us->_delete($delete_id);
			redirect('admin/contact_us');
		}			
			
	}	
	
	
	function submit()
	{		
	
		$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
			
		}
		else{
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
			
		}
		/*end of validation rule*/
		
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
				$data = $this->get_data_from_post();
				$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			
			
			redirect('admin/contact_us');
		}
			
	}	
	
	
					
//	function chpwd()
//	{
//		$update_id = $this->uri->segment(4);
//		if(isset($update_id) && is_numeric($update_id))
//			{
//				$chpwd_submit = $this->input->post('submit', TRUE);
//						
//				if($chpwd_submit=="Submit")
//					{
//					//person has submitted the form
//					$chpwd = $this->get_chpwd_from_post();
//					} 
//						
//				if(!isset($chpwd))
//					{
//					$chpwd = $this->get_data_from_post();
//					}
//						
//				$chdata['update_id'] = $update_id;										
//				$chdata['password'] = $chpwd;
//				
//				
//				$chdata['view_file'] = "admin/chpwd";
//				$this->load->module('template/admin_template');
//				$this->admin_template->admin($chdata);	
//				
//			}
//		else
//			{
//				redirect('admin/contact_us');
//			}	
//	}
//
//
//	function chpwd_submit()
//	{
//				
//	$this->load->library('form_validation');
//
//	$this->form_validation->set_rules('chpwd', 'New Password', 'required|xss_clean');
//
//		if ($this->form_validation->run($this) == FALSE)
//		{
//			$this->chpwd();
//		}
//		else
//		{
//			$chdata['password'] = $this->get_chpwd_from_post();
//			$update_id = $this->input->post('update_id', TRUE);
//			if(is_numeric($update_id)){		
//			
//				$pwd = $chdata['password'];
//				$chdata['password'] = $this->zenareta_pasaoro($pwd);
//				$this->chpwd_update($update_id, $chdata);
//			} else {
//				redirect('admin/membercategory');
//			}
//			
//			redirect('admin/contact_us');
//		}
//			
//	}
//		
		
	function get_chpwd_from_post()
	{
		$chpwd = $this->input->post('chpwd', TRUE);
		return $chpwd;
	}

	
	
	
	
	function get($order_by){
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get($order_by);
	return $query;
	}
	
	function get_membercategory()
	{
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get_membercategory_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
	
	function get_where($id){
            
	$this->load->model('mdl_contact_us');
	$query = $this->mdl_contact_us->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_insert($data);
	
	
	}

	function _update($id, $data){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->_delete($id);
	}
	
	function chpwd_update($id, $chpwd){
	$this->load->model('mdl_contact_us');
	$this->mdl_contact_us->chpwd_update($id, $chpwd);
	}
	
	function zenareta_pasaoro($pasaoro){		
	$this->load->model('admin_login/mdl_admin_login');
	$query = $this->mdl_admin_login->enc_hash_pwd($pasaoro);
	return $query;		
	}
	
	function sidebar($data){
	echo 'syo';
	}
	
}