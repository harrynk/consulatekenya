.<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contactus extends MX_Controller
{

	function __construct() {
	$this->load->model('mdl_contactus');
	parent::__construct();
	}

	function index(){
            /* for recaptcha*/
                $data =  $this->get_data_from_post();
                $this->load->library('recaptcha');
                $data['recaptcha_html']=$this->recaptcha->recaptcha_get_html();
            /*end of recaptcha*/
		
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);	
	}
        function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);			
		$data['slug'] = strtolower(url_title($data['name']));	
		$data['email'] = $this->input->post('email', TRUE);	
		$data['subject'] = $this->input->post('subject', TRUE);
                $data['description'] = $this->input->post('description', TRUE);
                $data['mailbody'] = $this->input->post('emailbody', TRUE);
		$data['ent_date'] = date("Y-m-d");
		$data['upd_date'] = NULL;
                
		return $data;
	}
        function create($status)
        {
		 /* for recaptcha*/
            if(!isset($status))
            {
            $status='';
            }
            $data =  $this->get_data_from_post();
            
            $data['message']=$status;
                 $this->load->library('recaptcha');
                 $data['recaptcha_html']=$this->recaptcha->recaptcha_get_html();
                /*end of recaptcha*/
		$data['view_file']="contactus";
                //$data['program_array'] = $this->get_program_if_live();
		$this->load->module('template');
		$this->template->front($data);	
	}
        function submit()
	{
         
	$this->load->library('recaptcha');
	$this->load->library('form_validation');
        $this->recaptcha->recaptcha_check_answer();
        if($this->recaptcha->getIsValid()){
       
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
                $this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');
                $this->form_validation->set_rules('emailbody', 'Message', 'required|xss_clean');
		/*end of validation rule*/
        }	
		
		if ($this->form_validation->run($this) == FALSE)
		{
                    $this->create('1');
		}
		else
		{
			$data = $this->get_data_from_post();
                        $this->send_email($data);
                        $this->_insert($data);
                        $this->create('0');
                        //redirect(base_url().'contactus');
		}
			
			//redirect($this->index());
                        
        }
        function send_email($data){
            $name = $data['name'] ;
            $email = $data['email'];
            $subject = $data['subject'];
            //var_dump($email,$name,$subject);
//            $emailbody ='Services ='.$data['programs'].'<br>'.$data['mailbody'] ;
            $emailbody = $data['mailbody'] ;
            $emailbody = 'Name='.$data['name'].'<br>Email='.$data['email'].'<br>Subject='.$data['subject'];
        

                        
                        $this->load->library('email');
                        $config['protocol'] = 'sendmail';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['charset'] = 'iso-8859-1';
                        $config['wordwrap'] = TRUE;
                        $config['mailtype'] = 'html';
                        
                        $this->email->initialize($config);
                        $this->email->from($email);
                        $this->email->to('bibek.munikar@gmail.com');
                        
//                        $this->email->cc($emailfrom);
                        //$this->email->bcc('them@their-example.com');
//$this->email->subject('INSEC SAM'."'".'S:'.$msg_from_name.':status:'.date('y-m-d',$data['ent_date']));
                        $this->email->subject('Trek himalayan'.':'.$subject);
                        $this->email->message('<div style="color:#003366">'.$emailbody.'<div>');
                        
//                        $this->email->subject('INSEC SAM'."'".'S:'.$name.':status:'.date('y-m-d',$data['ent_date']));
//                        $this->email->message($emailbody.'<div style="color:red"><div>');
//                                              

        
                        if (!$this->email->send())
                                {
                                   show_error($this->email->print_debugger());
                                } 
        }
        
        function get($order_by){
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_delete($id);
	}
        function generation_sitemap(){
		
		$data['view_file']="sitemap";
		$this->load->module('template');
		$this->template->front($data);	
	}
        
	
}