<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Positions</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/position/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>  
				
		
				    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name  <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control required"');?>
						</div> 
                    </div> 
<!--                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Phone_no <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('phone_no', $phone_no, 'class="form-control required"');?>
						</div> 
                    </div>
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Country  <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('country', $country, 'class="form-control required"');?>
						</div> 
                    </div>-->
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Email  <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('email', $email, 'class="form-control required"');?>
						</div> 
                    </div>
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Subject  <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('subject', $subject, 'class="form-control required"');?>
						</div> 
                    </div>
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Description  <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('description', $description, 'class="form-control required"');?>
						</div> 
                    </div>
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Address <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('address', $address, 'class="form-control required"');?>
						</div> 
                    </div>
                    			    
                <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>