<?php

if(!isset($message))
{
    $message=''; 
}
else 
{
    $message=$message;
}
?>
<script>
$( document ).ready(function() {
    var error='<?php echo $message;?>';
   if(error=='1')
   {
       // alert(error);
        
        $('#error').html("Please Enter CAPTCHA Correctly!!");
         
    }
    else if(error=='0')
    {
       $('#name').val('');
       $('#email').val('');
       $('#emailbody').val('');
       $('#subject').val('');
//       $('#volunteer_duration').val('');
//       $('#dob').val('');
//       $('#experience').val('');
        $('#success').html("Message Sent Successfully!!");
    }
    
});
</script>
<div class="col-lg-12">
    <div class="contact_us_form">
      <!-- <h3>Contact Us</h3>-->
       <p>To contact us please fill out the form below.</p>
            <?php
            echo form_open_multipart('contactus/submit', 'id="validate-1"');
            ?>
              <div class="form-group"> 
                <?php echo form_input('name', '', 'style = "" class="form-control" id="name" placeholder="Enter name"');?> 
              </div>
              <div class="form-group">
                <?php echo form_input('email', '', 'style = "" class="form-control" id="email" placeholder="Enter email"');?> 
              </div>
              <div class="form-group">
                <?php echo form_input('subject', '', 'style = "" class="form-control" id="subject" placeholder="Enter subject"');?> 
              </div>
              <div class="form-group">
                <?php
                   $emailbody = array(
                            'name'				=> 'emailbody',
                            'id'				=> 'emailbody',
                            'class'				=> 'form-control',
                            'placeholder'		=> 'Type the text to send'
                            );
                    echo form_textarea($emailbody);
                    ?>
              </div>
              <?php 
                echo $recaptcha_html;
                echo form_submit('save','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 

                ?>
                //<div class="clearfix"></div>
            <?php echo form_close() ?> 
                <hr/>
    </div>
    <div class=" contact_both">
        <div class="col-lg-6">
            <div class="contact_info">
                <h5>
                    <strong style="text-align:justify; color:#5c8d1c;">
                       Ayurveda Health Home(AHH)
                    </strong>
                </h5>
                <p style="font-size:16px;">Kathmandu</p> 


                <p class="author" style="text-align: justify;">
                    <strong>
                    Tel No: 01-4111111
                    Fax: 01-4111111
                    </strong>
                </p>
                <p>
                    <strong>
                    E-mail: info@ayurveda.com.np
                    </strong>
                </p>

            </div>

            <div class="contact_map">
                <iframe frameborder="0" height="300" width="250" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps/ms?hl=en&amp;ptab=2&amp;ie=UTF8&amp;oe=UTF8&amp;msa=0&amp;msid=200709510121314653374.0004976c4b52e9cc85a3e&amp;ll=27.740308,85.325875&amp;spn=0.004558,0.006866&amp;z=17&amp;output=embed"></iframe>
                <small>
                    <p><a href="https://www.google.com/maps/d/viewer?oe=UTF8&ll=27.740308,85.325875&source=embed&ie=UTF8&msa=0&spn=0.004558,0.006866&z=17&ptab=2&hl=en&mid=zC_RfN8q1kRM.kGPxhPyx6VdI" style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a></p>                </small>
            </div>
            
        </div>
        <div class="col-lg-6">
            <div class="contact_info">
                <h5>
                    <strong style="text-align:justify; color:#5c8d1c;">
                       Ayurveda Health Home(AHH)
                    </strong>
                </h5>
                <p style="font-size:16px;">Pokhara</p> 


                <p class="author" style="text-align: justify;">
                    <strong>
                    Tel No: 01-4111111
                    Fax: 01-4111111
                    </strong>
                </p>
                <p>
                    <strong>
                    E-mail: info@ayurveda.com.np
                    </strong>
                </p>

            </div>

            <div class="contact_map">
                <iframe frameborder="0" height="300" width="250" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps/ms?hl=en&amp;ptab=2&amp;ie=UTF8&amp;oe=UTF8&amp;msa=0&amp;msid=200709510121314653374.000497361d8d9b114cfcc&amp;ll=28.215579,83.960904&amp;spn=0.002269,0.003433&amp;z=18&amp;output=embed"></iframe>
                
                <small>
                    <p><a href="https://www.google.com/maps/d/viewer?oe=UTF8&ll=28.215579,83.960904&source=embed&ie=UTF8&msa=0&spn=0.002269,0.003433&z=18&ptab=2&hl=en&mid=zC_RfN8q1kRM.kDHFA9Tp6ZZg" style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a></p>
                </small>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script>
     $('#button').click(function()
    {
     $('#validate-1').submit();

    });
    
</script>