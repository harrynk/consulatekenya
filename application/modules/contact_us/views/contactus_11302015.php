<script>
$( document ).ready(function() {
    var error='<?php echo $message;?>';
   if(error=='1')
   {
       // alert(error);
        
        $('#error').html("Please Enter CAPTCHA Correctly!!");
         
    }
    else if(error=='0')
    {
       $('#name').val('');
       $('#email').val('');
       $('#emailbody').val('');
       $('#subject').val('');
//       $('#volunteer_duration').val('');
//       $('#dob').val('');
//       $('#experience').val('');
        $('#success').html("Message Sent Successfully!!");
    }
    
});
</script>
<?php 
if(!isset($message))
{
    $message=''; 
}
else 
{
    $message=$message;
}
?>
<div class="container">
        <div class="breadcrumb_div hidden-xs">  
                    <ul id="breadcrumbs" class="breadcrumb">  
                        <li>  
                            <i class="icon-home"></i>  
                            <a href="<?php echo base_url();?>">Home</a> 
                          </li>  
                        <li class="current"> 
                            <a>
                            <?php
                                        if($this->uri->segment(3))
                                        {
                                            $title_new=$this->uri->segment(3);
                                            $title =urldecode($title_new);
                                        }
                                        else
                                        {
                                            $title = $this->uri->segment(1);
                                        }
                                            if(strpos($title,'_') !=false)
                                            {
                                            $title_array=explode('_',$title);
                                            }
                                            else
                                            {
                                            $title_array=explode('-',$title);
                                            }

                                            foreach($title_array as $tarray)
                                            {
                                            echo ucwords(strtolower($tarray));
                                            echo ' ';
                                            }
                                                 //   echo $title_echo;
                                  ?>
                            </a>
                        </li> 	
                           

                    </ul>  
                </div>
    <div class="container">
        <div class="page_title">
<!--                            <h2>
                                <?php
                                        if($this->uri->segment(3))
                                        {
                                            $title_new=$this->uri->segment(3);
                                            $title =urldecode($title_new);
                                        }
                                        else
                                        {
                                            $title = $this->uri->segment(1);
                                        }
                                            if(strpos($title,'_') !=false)
                                            {
                                            $title_array=explode('_',$title);
                                            }
                                            else
                                            {
                                            $title_array=explode('-',$title);
                                            }

                                            foreach($title_array as $tarray)
                                            {
                                            echo ucwords(strtolower($tarray));
                                            echo ' ';
                                            }
                                                 //   echo $title_echo;
                                  ?>
                            </h2>-->
                        <h2 style="color: #e0730c;">Contact Us:</h2>
                        </div>
                <div class="col-md-5">
                    <div class="contact_info">
                        <h4>
                            <strong style="text-align:justify; color:#0072b1;">
                               Nagatours & Travels PVT. LTD.
                            </strong>
                        </h4>
                        <p style="font-size:16px;">Bhagawati Marg, Kathmandu 44600</p> 


                        <p class="author" style="text-align: justify;">
                            <strong>
                            Tel No: 01-4413931
                            </strong>
                        </p>
                        <p>
                            <strong>
                            E-mail: info@nagatours.com.np
                            </strong>
                        </p>

                    </div>

                    <div class="contact_map">
<!--                        <iframe frameborder="0" height="300" width="250" marginheight="0" marginwidth="0" scrolling="no" src="https://www.google.com.np/maps/place/TREK+HIMALAYAN+NEPAL+PVT.+LTD./@27.712447,85.3285042,18z/data=!4m2!3m1!1s0x0:0x5c655f5be23e17a9"></iframe>-->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.147570875195!2d85.3280858!3d27.7127296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb190eb9311945%3A0x5c655f5be23e17a9!2sTREK+HIMALAYAN+NEPAL+PVT.+LTD.!5e0!3m2!1sen!2s!4v1435903888246" width="90%" height="425" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <small>
                            <p style="margin-top:10px;"><a href="https://www.google.com.np/maps/place/TREK+HIMALAYAN+NEPAL+PVT.+LTD./@27.712447,85.3285042,18z/data=!4m2!3m1!1s0x0:0x5c655f5be23e17a9" style="color:#FFF;text-align:left" class="btn btn-danger" target="_blank">View Larger Map</a></p>                </small>
                    </div>

                </div>
            <div class="col-md-7">
              <!-- <h3>Contact Us</h3>-->
               <p style="font-size: 30px; color: #0072b1;">Quick Message</p>
                    <?php

                    echo form_open_multipart('contactus/submit', 'id="validate-1"');
                          ?>
                        <div id="error" style="color: red;"></div>
                        <div id="success" style="color: green;"></div>

                      <div class="form-group"> 
                          <label for="username">Name:</label>
                        <?php echo form_input('name', '', 'style = "width:90%" class="form-control" id="name" placeholder="Enter name"');?> 
                      </div>
                      <div class="form-group">
                          <label for="username">Email:</label>
                        <?php echo form_input('email', '', 'style = "width:90%" class="form-control" id="email" placeholder="Enter Email"');?> 
                      </div>
                      <div class="form-group">
                          <label for="username">Subject:</label>
                        <?php echo form_input('subject', '', 'style = "width:90%" class="form-control" id="subject" placeholder="Enter subject"');?> 
                      </div>
                      <div class="form-group">
                          <label for="username">Message</label>
                        <?php
                           $emailbody = array(
                                    'name'				=> 'description',
                                    'id'				=> 'emailbody',
                                    'class'				=> 'msz-box form-control',
                                    'rows'                              =>'6',
                                    'cols'                              =>'30',
                                    'placeholder'		=> 'Type the text to send'
                                    );
                            echo form_textarea($emailbody);
                            ?>
                      </div>
                      <?php 
                        echo $recaptcha_html;
                        ?>
                       <center> <?php echo form_submit('save','Submit','class="btn btn-primary"'); //name,value...type is default submit  ?></center>
                        <div class="clearfix"></div>
                    <?php echo form_close() ?> 
                        <hr/>
            </div>
                
    </div>
</div>
<script>
     $('#button').click(function()
    {
     $('#validate-1').submit();

    });
    
</script>