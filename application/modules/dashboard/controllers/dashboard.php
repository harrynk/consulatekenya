<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}
	
	function index(){		
		if ($this->session->userdata('user_id'))/*to check session*/
				{
					redirect('admin/dashboard');				
				}
			else
				{
					redirect('admin');
				}
		
		
	}

}