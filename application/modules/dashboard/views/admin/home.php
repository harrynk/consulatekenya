<div class="bs-docs-section">
    <div class="bs-glyphicons">
        <ul class="bs-glyphicons-list">
            <a href="<?php echo base_url();?>admin/news"><li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">News and Activities</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/photos">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Gallery Manager</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/photoscategory">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Gallery Folder</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/navigation/group/1">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Header Navigation</span>
            </li>
            </a>
            
            <a href="<?php echo base_url();?>admin/pages">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Page Management</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/contact_us">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Contact Us Message</span>
            </li>
            </a>
            
            <a href="<?php echo base_url();?>admin/links">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Links Management</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/publication">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Publication Management</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/news">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">News</span>
            </li>
            </a>
            
        </ul>
    </div>
</div>
<div class="clearfix"></div>