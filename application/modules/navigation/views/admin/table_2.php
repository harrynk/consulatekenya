<div class="manage">
    <input type="button" value="Add Nav" id="create" onclick="location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/create';"/> 
</div>
<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> <?php echo $group_title;?> </h4> <span style="font-size:10px; color:red;">[Usually, Childs are only created in header menu]</span>
	</div>
    <div class="widget-content">
    
        <style>
		ol{ list-style:none;}
		li div{
			border: 1px solid #d4d4d4;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			border-color: #D4D4D4 #D4D4D4 #BCBCBC;
			padding: 6px;
			margin: 0;
			cursor: move;
			}	
		.dd-handle{
			width:90%;
			}	
		</style>
		<?php
            echo form_open_multipart('admin/navigation/submit_nest', 'class="form-horizontal row-border" id="validate-1"');		
			
				
		//echo '<pre>'; print_r($query);die();		
        	$nav_array = $query;//becuz if(!empty($query->result())) returns error
               // var_dump($nav_array); die();
			if(!empty($nav_array)){ //checking if it is not empty i.e. if no navigation 
		?>   
        <div class="form-group"> <textarea name="jsoncode" id="nestable_list_1_output" class="form-control"></textarea> </div>
        <div class="dd" id="nestable_list_1">
            <ol class="dd-list">
            	<?php $i = 1;?>
                <?php foreach($nav_array as $row){?>
            		<li class="dd-item" data-id="<?php echo $row['position'];?>">
                                        	
                        <div class="dd-handle" style="float:left;">
                            <?php 
								echo $row['title'];
								$i++;
							?>
                        </div>  
                        <div style="float:none; border:none;">          
                            <button type="button" onclick="location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/create/<?php echo $row['id'];?>'"><i class="icon-pencil"></i></button>          
                            <button type="button" onclick="if (confirm('Are you sure, you want to delete it and all its child?')) location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/delete/<?php echo $row['id'];?>';"><i class="icon-trash"></i></button>
                            
                        </div>
                        
                        
                        
                        
                        
                        <?php // not to check if there is child or not
						if(isset($row['children'])){
                                                   // var_dump($row['children']);
						?>	
                        
                        <ol class="dd-list">
                        
							<?php
                            
                                    foreach($row['children'] as $child){
//                                        if (isset($row['children']['grand_children']))
//                                        {
                                           
                            ?>
                            
                                <li class="dd-item" data-id="<?php echo $child['position'];?>">
                                                    
                                    <div class="dd-handle" style="float:left;">
                                        <?php 
                                            echo $child['title'];
                                            $i++;
                                        ?>
                                    </div>  
                                    <div style="float:none; border:none;">          
                                        <button type="button" onclick="location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/create/<?php echo $child['id'];?>'"><i class="icon-pencil"></i></button>          
                                        <button type="button" onclick="if (confirm('Are you sure, you want to delete it and all its child?')) location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/delete/<?php echo $child['id'];?>';"><i class="icon-trash"></i></button>
                                        
                                    </div>
                                <ol class="dd-list">    
                                    <?php
//                                        }
                               if (isset($row['children']['grand_children']))
                                {
                                   foreach($row['children']['grand_children'] as $grand_child)
                                   {
                                     if ($grand_child['parent_id']==$child['id'])
                                     {
                                         
                                     ?>
                                         <li class="dd-item" data-id="<?php echo $grand_child['position'];?>">
                                                    
                                    <div class="dd-handle" style="float:left;">
                                        <?php 
                                            echo $grand_child['title'];
                                            $i++;
                                        ?>
                                    </div>  
                                    <div style="float:none; border:none;">          
                                        <button type="button" onclick="location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/create/<?php echo $grand_child['id'];?>'"><i class="icon-pencil"></i></button>          
                                        <button type="button" onclick="if (confirm('Are you sure, you want to delete it and all its child?')) location.href='<?php echo base_url()?>admin/navigation/group/<?php echo $group_id;?>/delete/<?php echo $grand_child['id'];?>';"><i class="icon-trash"></i></button>
                                        
                                    </div>
                                             
                                             <?php
                                     }
                                   }
                               }
                                    ?>
                                        </li>	
                                </ol>
                            <?php
							}
                            ?>    
                        </ol>    
                        
						<?php
							}// end of check, to see if there is child or not
						?>
                        
                        
                        
                        
                        
            
            		</li>
            	<?php }	?>                           
            </ol>
            
            <div class="form-actions"> 
				<?php 							
                    echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
                    $group_id = $this->uri->segment(4);
                    if (!empty($group_id)){echo form_hidden('group_id',$group_id);}	
                ?>
            </div> 
            
        </div>
        
        <?php 
			} //end of if(!empty($nav_array))
			echo form_close(); 
		?>  
    </div>

</div><!--end of class="widget box"-->
