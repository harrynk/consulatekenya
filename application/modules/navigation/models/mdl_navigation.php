<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_navigation extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}

	
	function get_table() {
	$table = "up_navigation";
	return $table;
	}
	
	function get_navigation_dropdown($group_id)
	{
//                $table = $this->get_table();
		$this->db->select('id, title');
//		$this->db->where('parent_id', '0');
		$this->db->where('group_id', $group_id);
		$this->db->order_by('title');
		$dropdowns = $this->db->get('up_navigation')->result();
		$dropdownlist[0] = '';//for making parent value null as default
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->title;
		}		
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
	
	function get_parentnav_dropdown()
	{
		$this->db->select('id, title, parent_id');
		$this->db->where('parent_id', '0');
		$this->db->order_by('title');
		$dropdowns = $this->db->get('up_navigation')->result();
		$dropdownlist[0] = '';//for making parent value null as default
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->title;
		}		
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
	
	function get_parent_name($group_id)
	{	
		$this->db->select('id, title');
		$this->db->where('group_id', $group_id);
		$this->db->where('parent_id', '0');
		$this->db->order_by('title');
		$dropdowns = $this->db->get('up_navigation')->result();		
		$dropdownlist[0] = '';//no name for blank
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->title;
		}		
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;		
	}
	
	
	
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_ind_nav($group_id){
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->order_by('position');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_parentnav($group_id){
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->where('parent_id', 0);
	$this->db->order_by('position');
	$query=$this->db->get($table);	
		$result = $query->result_array();
		if(empty($result)){return NULL;}
	return $result;
	}
			
	function get_childnav($group_id,$parent_id){
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->where('parent_id', $parent_id);	
	$this->db->order_by('position');
	$query=$this->db->get($table);
		$result = $query->result_array();
		if(empty($result)){return NULL;}
	return $result;
	}
	
	function get_parentnav_for_frontend($group_id){
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->where('parent_id', 0);
	$this->db->where('status', 'live');	
	//$this->db->order_by('position');
	$query=$this->db->get($table);	
		$result = $query->result_array();
//                var_dump($result); die;
		if(empty($result)){return NULL;}
	return $result;
	}
			
	function get_childnav_for_frontend($group_id,$parent_id){
        $table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->where('parent_id', $parent_id);	
	$this->db->where('status', 'live');		
	$this->db->order_by('position');
	$query=$this->db->get($table);
		$result = $query->result_array();
		if(empty($result)){return NULL;}
	return $result;
	}
	
	function check_if_nav_belongs_to_group($group_id,$id){
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->where('id', $id);
	$query=$this->db->get($table);	
	$arr = $query->result_array();
	if(empty($arr)){return FALSE;}
	else{return TRUE;}
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	
	$this->db->where('parent_id', $id);		//deleting all the child navigation of the 	
	$this->db->delete($table);				//particular navigation.
	}
	
	
	function get_id_of_parent_from_position_number($position){
	$table = $this->get_table();
	$this->db->select('id');
	$this->db->where('position', $position);
	$query=$this->db->get($table);
	$result = $query->result();
	return $result[0]->id;
	}
	
	function update_parent_id_from_position($parent_id,$position,$group_id){
	$table = $this->get_table();
	$this->db->where('position', $position);
	$this->db->where('group_id', $group_id);
	$this->db->update($table, array('parent_id' => $parent_id));
	}
	
	function update_nest($position_array){
	$table = $this->get_table();
		foreach ($position_array as $row){	
			$this->db->where('position', $row);
			$this->db->update($table, array('position' => 'li'.$row));
		}
		$i=1;
		foreach ($position_array as $row){	
			$this->db->where('position', 'li'.$row);
			$this->db->update($table, array('position' => $i));
			$i++;			
		}
	}
	
	function get_next_position($group_id){	
	$table = $this->get_table();
	$this->db->select('position');
	$this->db->where('group_id', $group_id);
	$query=$this->db->get($table);
	$result = $query->result();
	if(empty($result)){return 1;}//because if there is no any navigation of current group we'd like to set it 1(i.e. 1st)
	$i=0;$new_array = array();	
		foreach ($result as $row)
		{
			$new_array[$i] = $row->position;
			$i++;
		}	
	$max_position = max($new_array);
	$next_position = $max_position + 1;
	return $next_position;
	}
	
	
	
	function get_navigation_from_navigation_name($navigation_name){	
	$navigation_id = $this->get_navigation_id_from_navigation_name($navigation_name);
	$table = $this->get_table();
	$this->db->where('status', 'live');
	$this->db->where('group_id', $navigation_id); 
	$query=$this->db->get($table);
	return $query;		
	}
	
	function get_navigation_id_from_navigation_name($navigation_name){
	$this->db->select('id');
	$this->db->like('title', $navigation_name);
	$query=$this->db->get('up_navigation_group')->result_array();
	return $query[0]['id'];
	}
        function get_parent_slug($slug)
        {
            $this->db->select('id, parent_id,title,slug');
            $this->db->where('slug', $slug);
            $this->db->where('group_id', '1');
            $dropdowns = $this->db->get('up_navigation')->result_array();
            if(isset($dropdowns[0]['parent_id']))
            {
            if($dropdowns[0]['parent_id']==0 ){
               return $dropdowns; 
            }
//            if($dropdowns[0]['slug']->num_rows())
            $this->db->select('id, parent_id,title,slug');
            $this->db->where('id', $dropdowns[0]['parent_id']);
            $querys=$this->db->get('up_navigation')->result_array();
            return $querys;
            }
            return '';
        }
        function get_childnav_for_frontend_navigation($slug){
        $table = $this->get_table();
        $this->db->select('n.*');
        $this->db->join('up_navigation n2','n.parent_id=n2.id');
        $this->db->join('up_pages','n2.page_id=up_pages.id');
        $this->db->where('up_pages.slug',$slug);
        $query=$this->db->get('up_navigation n');
        return $query;
        }
        	function get_parentid()
	{
	$page_slug = $this->uri->segment(1);
	$this->db->select('up_navigation.*');
	$this->db->join('up_pages', 'up_pages.id = up_navigation.page_id');
	$this->db->where('up_pages.slug', $page_slug);
	$result=$this->db->get('up_navigation')->result();
	return $result;
	}
	
}