<?php 
$error_msz = $this->session->flashdata('error');
if(!empty($error_msz)){?>
	<div class="alert alert-danger fade in">
        <i class="icon-remove close" data-dismiss="alert"></i> 
        <strong>Error!</strong> <?php echo $error_msz;?>
	</div>		
<?php }?>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> User Permissions </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Group</th>  
                    <th>Set Permission</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo Ucfirst($row->name);?></td>
                    <td>
                    	<?php if($row->id != '1'){
							echo '<a href="'.base_url().'admin/permissions/group/'.$row->id.'"><i class="icon-key"></i>  Set Permission</a>';
							}
							else{ echo "The Admin group has access to everything"; }
						?>             
                    </td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->