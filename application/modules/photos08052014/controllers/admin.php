<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('photos'); //module name is photos here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);
                $data['category_id'] = $this->input->post('category_id', TRUE);
                $data['description'] = $this->input->post('description', TRUE);
		$data['status'] = $this->input->post('status', TRUE);	
		$data['slug'] = strtolower(url_title($data['name']));
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
			
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
			$data['category_id'] = $row->category_id;
                        $data['description'] = $row->description;
			$data['attachment'] = $row->attachment;
			$data['status'] = $row->status;		
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['group_array'] = $this->get_groups();		
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_photos');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/photos');
			}
		else
		{
			$this->mdl_photos->_delete($delete_id);
			redirect('admin/photos');
		}				
	}
	
	
	function submit()
	{		
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
                       $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
		}
		else{
                       $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
                       $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
                
                }
		/*end of validation rule*/	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){						
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];}
				
				$this->_update($update_id, $data);
			} else {								
				
				$nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				
				$this->_insert($data);	
			}
		
		redirect('admin/photos');	
			
	}
	
	
	
	
	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/photos/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_photos');
	$id = $this->mdl_photos->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_photos');
	$query = $this->mdl_photos->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_photos');
	$query = $this->mdl_photos->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_photos');
	$this->mdl_photos->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_photos');
	$this->mdl_photos->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_photos');
	$this->mdl_photos->_delete($id);
	}
        function get_groups()
	{
	$this->load->model('photoscategory/mdl_photoscategory');
	$query = $this->mdl_photoscategory->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
}