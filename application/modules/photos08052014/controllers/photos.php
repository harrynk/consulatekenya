<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class photos extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_photos');
		parent::__construct();
	}


	function index(){
                $this->load->model('photoscategory/mdl_photoscategory');
                $photos = $this->mdl_photoscategory->get_front('id');
                $data['photos'] = $photos;
                $data['view_file']="table";
                $this->load->module('template');
                $this->template->front($data);
	}
	
	function details()
	{ 
                $slug = $this->uri->segment(3);
                //$id = $this->uri->segment(3);
                $photos = $this->mdl_photos->get_photos($slug);
                $data['photos'] = $photos;
                $data['view_file']="form";
                $this->load->module('template');
                $this->template->front($data);
	}
       
}