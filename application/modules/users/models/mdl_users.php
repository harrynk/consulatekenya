<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_users extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}
	
	function get_table() {
	$table = "up_users";
	return $table;
	}
	
	
	function get($order_by){
	$table = $this->get_table();	
	$this->db->select('up_users.*');
	$this->db->select('up_users_groups.name as group_name');
	$this->db->join('up_users_groups', 'up_users_groups.id = up_users.group_id');
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
	
	function get_username_from_user_id($user_id){
	$table = $this->get_table();
	$this->db->select('display_name');
	$this->db->order_by('id');
	$query=$this->db->get($table)->result();
	return $query[0]->display_name;
	}
	
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	function chpwd_update($id, $chpwd){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $chpwd);
	}
	
}