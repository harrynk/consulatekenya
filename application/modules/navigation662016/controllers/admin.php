<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

   public function __construct()
   {
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('navigation'); //module name is navigation here	
   }
   
	//-------------------start of group nav----------------------//	
	function index(){
		$data['query'] = $this->get_group('id');
		
		$data['view_file'] = "admin/group_table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}
	
	function create_group(){
		$update_id = $this->uri->segment(4);
		if($update_id != 1 && $update_id != 2){ //because 1 is header and 2 is footer which i don't want to change or edit or delete
			$submit = $this->input->post('submit', TRUE);
			
				if($submit=="Submit"){
					//person has submitted the form
					$data = $this->get_data_from_post();
				} 
				else {
					if (is_numeric($update_id)){
						$data = $this->get_data_from_db($update_id);
					}
				}
				
				if(!isset($data)){
				$data = $this->get_data_from_post();
				}
			
					
			$data['update_id'] = $update_id;		
			$data['view_file'] = "admin/group_form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
		}else{
		redirect('admin/navigation');
		}
	}	
	
	
	function delete_group(){
		$this->load->model('mdl_navigation_group');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/navigation');
			}
		else
		{
			$this->mdl_navigation_group->_delete_group($delete_id);
			redirect('admin/navigation');
		}
						
		$data['view_file'] = "admin/group_table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
					
	}
	
	function get_data_from_post() {
		
		$data['title'] = $this->input->post('title', TRUE);	
		$data['slug'] = strtolower(url_title($data['title']));
		return $data;		
	}
	
	function get_data_from_db($update_id){
		$query = $this->get_where_group($update_id);
		foreach($query->result() as $row){
			$data['title'] = $row->title;
			$data['slug'] = $row->slug;
		}
		
		if(!isset($data)){
				$data = "";
		}
		return $data;
	}
	
	function submit_group(){
		
		$update_id = $this->input->post('update_id', TRUE);
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			if(is_numeric($update_id)){		
				$this->_update_group($update_id, $data);
			} else {
				$this->_insert_group($data);
			}
			
			redirect('admin/navigation');
		}
			
	}

	function get_group($order_by){
	$this->load->model('navigation/mdl_navigation_group');
	$query = $this->mdl_navigation_group->get_group($order_by);
	return $query;
	}
	
	function get_where_group($id){
	$this->load->model('mdl_navigation_group');
	$query = $this->mdl_navigation_group->get_where_group($id);
	return $query;
	}
	
	function _insert_group($data){
	$this->load->model('mdl_navigation_group');
	$this->mdl_navigation_group->_insert_group($data);
	}

	function _update_group($id, $data){
	$this->load->model('mdl_navigation_group');
	$this->mdl_navigation_group->_update_group($id, $data);
	}
	
	function _delete_group($id){
	$this->load->model('mdl_navigation_group');
	$this->mdl_navigation_group->_delete_group($id);
	}
	//-------------------end of group nav----------------------//
	
	
	
	
	
	
	//-------------------------------------------------start of individual navigation----------------------------------------------------//
	

	function group(){//to list out every navigation individually
		$manage = $this->uri->segment(5);	
		if(empty($manage)){
			$group_id = $this->uri->segment(4);	
			$existence = $this->check_group_existence($group_id);
			if($existence == FALSE){ redirect('admin/navigation');}//checking if the group_id exist or not
			else
			{	
				$data['query'] = $this->all_nav($group_id);	
						
				$data['parent_name'] = $this->get_parent_name($group_id);
				$data['group_id'] = $group_id;
				$data['group_title'] = $this->get_group_title($group_id);
				$data['view_file'] = "admin/table";
				$this->load->module('template/admin_template');
				$this->admin_template->admin($data);				
			}
		}else{
				$this->$manage();
		}
	}
	
	function check_group_existence($id){
		$this->load->model('mdl_navigation_group');
		$query = $this->mdl_navigation_group->check_group_existence($id);
		return $query;
	}
	
	function check_if_nav_belongs_to_group($group_id,$nav_id){
		$this->load->model('mdl_navigation');
		$query = $this->mdl_navigation->check_if_nav_belongs_to_group($group_id,$nav_id);
		return $query;
	}
	
	function all_nav($group_id){//to get navigation list of specific group
					
		$data['parentnav'] = $this->get_parent($group_id);	//
		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
				$nav['children'] = $children;
			}
		$navigation[$i] = $nav;
		$i++;	
		}
	return $navigation;
	}
	
	function get_ind_nav($group_id){//to get navigation list of specific group
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_ind_nav($group_id);
	return $query;
	}
	
	function get_parent($group_id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_parentnav($group_id);	
	return $query;
	}
	
	function get_child($group_id,$parent_id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_childnav($group_id,$parent_id);
	return $query;
	}
	
	function get_group_title($group_id){//to get navigation title of specific group
	$this->load->model('mdl_navigation_group');
	$query = $this->mdl_navigation_group->get_group_title($group_id);
	return $query;
	}
	
	
	function create(){
		$update_id = $this->uri->segment(6);
		$group_id = $this->uri->segment(4);
		$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_ind_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					
					$group_id = $this->uri->segment(4);
					$nav_id = $this->uri->segment(6);
					$check_status = $this->check_if_nav_belongs_to_group($group_id,$nav_id);
					
						if($check_status == TRUE){
					
							$data = $this->get_ind_data_from_db($update_id);
					
						}else{
						redirect('admin/navigation/group/'.$group_id);
						}					
					
				}
			}
			
			if(!isset($data)){
			$data = $this->get_ind_data_from_post();
			}
		
				
		$data['update_id'] = $update_id;
		$data['parent'] = $this->get_navigation($group_id);
		$data['modulelist'] = $this->get_moduleslist();
		$data['pagelist'] = $this->get_webpages();		
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}
	
	function delete()
	{
		$this->load->model('mdl_navigation');
		$delete_id = $this->uri->segment(6);	
		$group_id = $this->uri->segment(4);
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/navigation/group/'.$group_id);
			}
		else
		{
			$this->mdl_navigation->_delete($delete_id);
				redirect('admin/navigation/group/'.$group_id);
		}
					
	}	
	
	function submit(){
		
		$update_id = $this->input->post('update_id', TRUE);
			
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_ind_data_from_post();
			
			if($data['navtype'] == 'Module'){$data['page_id'] = 'NULL';$data['link_url'] = 'NULL';}
			elseif($data['navtype'] == 'Page'){$data['module_id'] = 'NULL';$data['link_url'] = 'NULL';}
			else{$data['module_id'] = 'NULL';$data['page_id'] = 'NULL';}
			
			if(is_numeric($update_id)){			
				$this->_update($update_id, $data);
			} else {			
			
				$new_position = $this->get_next_position($data['group_id']);//becuz we have to set the position as well
				$data['position'] = $new_position;							//and the next position will be be fore new nav
				
				$this->_insert($data);
			}
			
			redirect('admin/navigation/group/'.$data['group_id']);
		}
			
	}
	
	
	function submit_nest(){
		$new_array = $_POST;
		$group_id = $new_array['group_id'];
		$jsoncode = json_decode($new_array['jsoncode'], true, 64);
		$i=1;$position_array = array();
			foreach($jsoncode as $row){
				$parent_id = 0;
				$position = $row['id'];
				$position_array[$i] = $row['id'];
				$i++;
				$this->update_parent_id_from_position($parent_id,$position,$group_id);
				
				if(isset($row['children'])){
					foreach($row['children'] as $child){						
						$parent_id_for_children = $this->get_id_of_parent_from_position_number($row['id']);
						$parent_id = $parent_id_for_children;		
						$position = $child['id'];
						$position_array[$i] = $child['id'];
						$i++;
						$this->update_parent_id_from_position($parent_id,$position,$group_id);	
					}
				}			
			}
			
		
		$submit = $new_array['submit'];
		unset($new_array['group_id']);
		unset($new_array['submit']);
				
		$this->update_nest($position_array);
		redirect('admin/navigation');		
	}
	
	function get_id_of_parent_from_position_number($position){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_id_of_parent_from_position_number($position);
	return $query;				
	}
	
	function update_parent_id_from_position($parent_id,$position,$group_id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->update_parent_id_from_position($parent_id,$position,$group_id);
	return $query;
	}

	function update_nest($position_array){
	$this->load->model('mdl_navigation');
	$this->mdl_navigation->update_nest($position_array);
	}
	
	function get_ind_data_from_post() {
		
		$data['title'] = $this->input->post('title', TRUE);
		$data['slug'] = strtolower(url_title($data['title']));
		$data['navtype'] = $this->input->post('navtype', TRUE);
		$data['parent_id'] = $this->input->post('parent_id', TRUE);	
		$data['group_id'] = $this->input->post('group_id', TRUE);	
		$data['module_id'] = $this->input->post('module_id', TRUE);	
		$data['page_id'] = $this->input->post('page_id', TRUE);	
		$data['site_uri'] = $this->input->post('site_uri', TRUE);	
		$data['link_url'] = $this->input->post('link_url', TRUE);
		$data['status'] = $this->input->post('status', TRUE);	
		return $data;		
	}
	
	function get_next_position($group_id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_next_position($group_id);	
	return $query;	
	}
	
	function get_ind_data_from_db($update_id){
		$result = $this->mdl_navigation->get_where_backend($update_id);
		//print_r($result);
		foreach($result as $row){
			$data['title'] = $row->title;
			$data['slug'] = $row->slug;
			$data['navtype'] = $row->navtype;
			$data['parent_id'] = $row->parent_id;
			$data['group_id'] = $row->group_id;	
			$data['module_id'] = $row->module_id;
			$data['page_id'] = $row->page_id;
			$data['site_uri'] = $row->site_uri;
			$data['link_url'] = $row->link_url;
			$data['status'] = $row->status;		
		}
		
		if(!isset($data)){
				$data = "";
		}
		return $data;
	}
	
	function get_navigation($group_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_navigation_dropdown($group_id);
	return $query;
	}
	
	function get_parent_name($group_id){	
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_parent_name($group_id);
	return $query;			
	}
	
	function get_moduleslist()
	{
		$this->load->model('modules/mdl_moduleslist');
		$query = $this->mdl_moduleslist->get_modules_dropdown();
		return $query;
	}
	
	function get_webpages()
	{
		$this->load->model('pages/mdl_pages');
		$query = $this->mdl_pages->get_pages_dropdown();
		return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_navigation');
	$query = $this->mdl_navigation->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_navigation');
	$this->mdl_navigation->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_navigation');
	$this->mdl_navigation->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_navigation');
	$this->mdl_navigation->_delete($id);
	}
	
	
	
	//-------------------------------------------------end of individual navigation----------------------------------------------------//
}