<div class="col-lg-12 background-ffffff footer-top">
	<h3><?php echo $title;?></h3>
    
    <img src="<?php echo base_url();?>uploads/news/<?php echo $attachment;?>" class="img-responsive thumbnail" style="width:60%;" alt="" />
    <div class="date_entry" style="font-size:13px;"><?= date('F j,Y',strtotime($published_date)); ?></div>
    <p><?= $description; ?></p>
</div>