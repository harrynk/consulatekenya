<div class="col-lg-12 background-ffffff footer-top">
	<h3><?php echo $title;?></h3>
    <div class="date_entry"><?= date('F j,Y',strtotime($published_date)); ?></div>
    <p><?= $description; ?></p>
</div>