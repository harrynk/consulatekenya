<div class="col-lg-3 background-fcfcfc">
	<ul class="sidenav">
		<?php
            foreach($side as $header){
        ?>
                <?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                    <li><a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?></a></li>                              
                <?php 
                
            }

        ?>
    </ul>
</div>
<div class="col-lg-9">
    <div class="recent_news">
        <h3>Recent News</h3>
        <?php
            foreach($news as $singnews)
            {
        ?>
        <h5><?php echo $singnews->title; ?></h5>
        <p><?php echo substr($singnews->description,0,500).'...'; ?> 
        <span class="read_more"><a href="<?php echo base_url().'news/details/'.$singnews->slug;?>">read more</a></span>
        </p>
        <hr />
        <?php
            }
        ?>
    </div>
</div>