<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news extends MX_Controller
{

	function __construct() {
            
		$this->load->model('mdl_news');
		parent::__construct();
	}

function index(){
            
		$news = $this->get('id');
		$data['side'] = $this->get_child(1,1);
        $data['news']=$news;
		$data['view_file']="table";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function get_name_from_module($module_id){		
	$this->load->model('template/mdl_template');
	$query = $this->mdl_template->get_name_from_module($module_id);	
	return $query['0']['slug'];	
	}
	
	function get_name_from_page($page_id){		
	$this->load->model('template/mdl_template');
	$query = $this->mdl_template->get_name_from_page($page_id);	
	return $query['0']['slug'];		
	}
	
	function add_href($result){
		$count = count($result);
		for($i=0;$i<$count;$i++){			
				if($result[$i]['navtype'] == 'Module'){$result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'Page'){$result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'URI'){$result[$i]['href'] = $result[$i]['site_uri'];$result[$i]['target'] = "_self";}
				
				else{$result[$i]['href'] = $result[$i]['link_url'];$result[$i]['target'] = "_blank";}			
		}
		return $result;
	}
	
	function get_child($group_id,$parent_id){
		$this->load->model('navigation/mdl_navigation');
		$query = $this->mdl_navigation->get_childnav_for_frontend($group_id,$parent_id);	
		$result = $this->add_href($query);
		return $result;
	}
	
function details()
	{ 
		$slug = $this->uri->segment(3);
                
		$query= $this->get_where($slug);
                foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
                        $data['description'] = $row->description;
                       $data['published_date'] = $row->published_date;
                       $data['attachment'] = $row->attachment;
		}
                
                
               
		$data['view_file']="form";
		$this->load->module('template');
		$this->template->front($data);
	}
         function get_where($id){
	
	$query = $this->mdl_news->get_where($id);
	return $query;
	}
    
	function get($order_by){
	$this->load->model('mdl_news');
	$query = $this->mdl_news->get_front($order_by);
	$result = $query->result();
	return $result;
	}
}
